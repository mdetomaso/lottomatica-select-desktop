import {Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {CausaliInterface, LtmSelectComponent} from '../ltm-select/ltm-select.component';
import {LtmSelectDateComponent} from '../ltm-select-date/ltm-select-date.component';

@Component({
    selector: 'app-ltm-filter-wrapper',
    templateUrl: './filter-wrapper.component.html',
    styleUrls: ['./filter-wrapper.component.scss']
})
export class FilterWrapperComponent implements OnInit {
    @ViewChild('select1', {static: false}) select1: LtmSelectComponent;
    @ViewChild('select2', {static: false}) select2: LtmSelectComponent;
    @ViewChild(LtmSelectDateComponent, {static: false}) selectDate: LtmSelectDateComponent;
    miniGames = false;
    public innerWidth: any;

    causaliList: CausaliInterface[] = [
        {name: 'Bonus', id: 'A', checked: false},
        {name: 'Deposito', id: 'B', checked: false},
        {name: 'Account management', id: 'C', checked: false},
        {name: 'Giocata Bingo', id: 'D', checked: false},
        {name: 'Giocata Casinò', id: 'E', checked: false},
        {name: 'Giocata Scommesse', id: 'F', checked: false},
        {name: 'Giocata Virtual', id: 'G', checked: false},
        {name: 'Giocata Eurojackpot', id: 'H', checked: false},
        {name: 'Giocata Fantasysport', id: 'I', checked: false},

        {name: 'NOME BUFFO 1', id: 'J', checked: false},
        {name: 'NOME BUFFO 2', id: 'K', checked: false},
        {name: 'NOME BUFFO 3', id: 'L', checked: false},
        {name: 'NOME BUFFO 4', id: 'N', checked: false},
        {name: 'NOME BUFFO 5', id: 'O', checked: false},
        {name: 'NOME BUFFO 6', id: 'P', checked: false}
    ];
    statoGiocateList: CausaliInterface[] = [
        {name: 'Vincente', id: 'A', checked: false},
        {name: 'Non Vincente', id: 'B', checked: false},
        {name: 'In Corso', id: 'C', checked: false},
        {name: 'Rimborsato', id: 'D', checked: false},
    ];

    constructor() {
    }

    ngOnInit(): void {
        this.innerWidth = window.innerWidth;
    }

    resetFiltri() {
        this.select1.clear();
        this.select2.clear();
        this.selectDate.clear();
        this.miniGames = false;
    }

    onChecked() {
        this.miniGames = !this.miniGames;
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.innerWidth = window.innerWidth;
    }

}
