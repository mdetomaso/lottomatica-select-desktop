import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import moment from 'moment';

@Component({
    selector: 'app-ltm-datepiker-dialog',
    templateUrl: './datepiker-dialog.component.html',
    styleUrls: ['./datepiker-dialog.component.scss']
})
export class DatepikerDialogComponent implements OnInit {
    form: FormGroup;
    startDate = '';
    endDate = '';

    endDateIsBetweenStartAndToday = true;
    startDateIsAfterToday = false;
    endDateIsAfterToday = false;
    errorValidityStartDate = false;
    errorValidityEndDate = false;


    today = moment(new Date());

    startDateMoment = moment(new Date());
    endDateMoment = moment(new Date());

    localeOptions = {
        direction: 'ltr',
        weekLabel: 'W',
        daysOfWeek: [
            'D',
            'L',
            'M',
            'M',
            'G',
            'V',
            'S'
        ],
        firstDay: 1,
        monthNames: [
            'GENNAIO',
            'FEBBRAIO',
            'MARZO',
            'APRILE',
            'MAGGIO',
            'GIUGNO',
            'LUGLIO',
            'AGOSTO',
            'SETTEMBRE',
            'OTTOBRE',
            'NOVEMBRE',
            'DICEMBRE'
        ]
    };

    constructor(
        public dialogRef: MatDialogRef<DatepikerDialogComponent>,
        private fb: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {

        moment().locale('it');

        if (this.data.startDate && this.data.endDate) {
            const dateStart = this.data.startDate.split('/');
            const dateEnd = this.data.endDate.split('/');
            this.startDateMoment = moment([+dateStart[2], +dateStart[1] - 1, +dateStart[0]]);
            this.endDateMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);

            this.startDate = this.data.startDate;
            this.endDate = this.data.endDate;
        }

        this.form = this.fb.group({
            data1: [this.startDate],
            data2: [this.endDate],
        });
    }

    close(startDate?: string, endDate?: string): void {
        if (startDate && endDate) {
            this.dialogRef.close({date: startDate + ' - ' + endDate});
        } else {
            this.dialogRef.close();
        }
    }

    ngOnInit(): void {
    }
    startDateChange(event) {
        this.startDate = moment(event.startDate).format('DD/MM/YYYY');
        this.endDate = '';
    }

    endDateChange(event) {
    }
    choosedDate(evento: any) {
        if (evento.chosenLabel) {
            let dal = evento.chosenLabel.slice(0, 10);
            let al = evento.chosenLabel.slice(13);
            dal = dal.slice(3, 6) + dal.slice(0, 3) + dal.slice(6);
            al = al.slice(3, 6) + al.slice(0, 3) + al.slice(6);
            this.startDate = dal;
            this.endDate = al;

            const dateStart = this.startDate.split('/');
            this.startDateMoment = moment([+dateStart[2], +dateStart[1] - 1, +dateStart[0]]);
            this.startDateIsAfterToday = this.startDateMoment.isAfter(this.today);

            const dateEnd = this.endDate.split('/');
            this.endDateMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);
            this.endDateIsAfterToday = this.endDateMoment.isAfter(this.today);
            if (this.startDateMoment) {
                this.endDateIsBetweenStartAndToday = this.endDateMoment.isBetween(this.startDateMoment, this.today, undefined, '[]');
            }
        }
    }

    get errorsValidity() {
        if (this.startDate.length === 10 && this.endDate.length === 10) {
            if (!this.errorValidityStartDate && !this.errorValidityEndDate) {
                return this.startDateIsAfterToday || this.endDateIsAfterToday || !this.endDateIsBetweenStartAndToday;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }



    onChangeStartDate(startDate) {
        if (startDate) {
            if (startDate.length === 2) {
                this.startDate += '/';
            } else if (startDate.length === 5) {
                this.startDate += '/';
            } else if (startDate.length === 10) {
                const dateStart = this.startDate.split('/');
                this.startDateMoment = moment([+dateStart[2], +dateStart[1] - 1, +dateStart[0]]);
                if (this.startDateMoment.isValid()) {
                    this.errorValidityStartDate = false;
                    this.startDateIsAfterToday = this.startDateMoment.isAfter(this.today);
                } else {
                    this.errorValidityStartDate = true;
                }

            }
        }
    }

    onChangeEndDate(endDate) {
        if (endDate) {
            if (endDate.length === 2) {
                this.endDate += '/';
            } else if (endDate.length === 5) {
                this.endDate += '/';
            } else if (endDate.length === 10) {
                const dateEnd = this.endDate.split('/');
                this.endDateMoment = moment([+dateEnd[2], +dateEnd[1] - 1, +dateEnd[0]]);
                if (this.endDateMoment.isValid()) {
                    this.endDateIsAfterToday = this.endDateMoment.isAfter(this.today);
                    if (this.startDateMoment) {
                        this.errorValidityEndDate = false;
                        this.endDateIsBetweenStartAndToday =
                            this.endDateMoment.isBetween(this.startDateMoment, this.today, undefined, '[]');
                    }
                } else {
                    this.errorValidityEndDate = true;
                }
            }
        }
    }

}
