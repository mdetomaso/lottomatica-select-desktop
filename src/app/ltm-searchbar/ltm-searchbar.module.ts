import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilterWrapperComponent} from './filter-wrapper/filter-wrapper.component';

import {LtmSelectComponent} from './ltm-select/ltm-select.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {LtmSelectDateComponent} from './ltm-select-date/ltm-select-date.component';
import {RouterModule, Routes} from '@angular/router';
import {MatDialogModule} from '@angular/material/dialog';
import {DatepikerDialogComponent} from './datepiker-dialog/datepiker-dialog.component';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material';
import {MatInputModule} from '@angular/material/input';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {OnlynumberDirective} from '../core/directives/onlynumber.directive';
import {MaterialModule} from '../material.module';
import {MatIconModule} from '@angular/material/icon';

const routes: Routes = [
    {
        path: '',
        component: FilterWrapperComponent
    }
];

@NgModule({
    declarations: [
        DatepikerDialogComponent,
        LtmSelectComponent,
        FilterWrapperComponent,
        LtmSelectDateComponent,
        OnlynumberDirective
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(routes),
        NgxMatSelectSearchModule,
        NgMultiSelectDropDownModule.forRoot(),
        NgxDaterangepickerMd.forRoot(),
        MaterialModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatIconModule
    ],
    exports: [
        FilterWrapperComponent,
        LtmSelectComponent,
        LtmSelectDateComponent
    ],
    entryComponents: [
        DatepikerDialogComponent
    ]
})
export class LtmSearchbarModule {
}
