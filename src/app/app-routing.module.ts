import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/ltm-searchbar',
        pathMatch: 'full'
    },
    {
        path: 'ltm-searchbar',
        loadChildren: './ltm-searchbar/ltm-searchbar.module#LtmSearchbarModule'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
