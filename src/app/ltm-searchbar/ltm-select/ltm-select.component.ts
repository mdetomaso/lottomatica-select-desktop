import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild, ViewEncapsulation} from '@angular/core';
import {LtmSearchbarMobileService} from '../../core/services/ltm-searchbar-mobile.service';
import {Router} from '@angular/router';
import {take, takeUntil} from 'rxjs/operators';
import {ReplaySubject, Subject} from 'rxjs';
import {MatSelect} from '@angular/material/select';

export interface CausaliInterface {
    id: string;
    name: string;
    checked: boolean;
}

@Component({
    selector: 'app-ltm-select',
    templateUrl: './ltm-select.component.html',
    styleUrls: ['./ltm-select.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LtmSelectComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

    @Input() label: string;
    @Input() isFullScreen = false;
    @Input() isCausali = true;
    optionsList: CausaliInterface[] = [];
    /*conto causali*/
    causaliSelectCompleted;
    selected;

    causaliList: CausaliInterface[] = [
        {name: 'Bonus', id: 'A', checked: false},
        {name: 'Deposito', id: 'B', checked: false},
        {name: 'Giocata Bingo', id: 'D', checked: false},
        {name: 'Giocata Casinò', id: 'E', checked: false},
        {name: 'Giocata Scommesse', id: 'F', checked: false},
        {name: 'Giocata Virtual', id: 'G', checked: false},
        {name: 'Giocata Eurojackpot', id: 'H', checked: false},
        {name: 'Giocata Fantasysport', id: 'I', checked: false},
        ...altreCausali1,
        ...altreCausali2,
        ...altreCausali3,
        ...altreCausali4,
        ...altreCausali5
    ];
    statoGiocateList: CausaliInterface[] = [
        {name: 'Vincente', id: 'A', checked: false},
        {name: 'Non Vincente', id: 'B', checked: false},
        {name: 'In Corso', id: 'C', checked: false},
        {name: 'Rimborsato', id: 'D', checked: false},
        {name: 'Non Rimborsato', id: 'E', checked: false},
    ];


    public bankMultiCtrl: FormControl = new FormControl();
    public bankMultiFilterCtrl: FormControl = new FormControl();
    public filteredBanksMulti: ReplaySubject<CausaliInterface[]> = new ReplaySubject<CausaliInterface[]>(1);

    @ViewChild('multiSelect', {static: true}) multiSelect: MatSelect;

    // tslint:disable-next-line:variable-name
    protected _onDestroy = new Subject<void>();
    public allBanksSize;


    form: FormGroup;
    isCountVisible = true;

    constructor(
        public ltmSearchbarMobileService: LtmSearchbarMobileService,
        private router: Router,
        private fb: FormBuilder
    ) {
        this.bankMultiCtrl.setValue([{id: 'selectAll', name: ''}]);
    }

    ngOnInit(): void {
        this.optionsList = this.isCausali ? [...this.causaliList] : [...this.statoGiocateList];
        if (this.isCausali) {
            this.selected = altreCausali1.length +
                altreCausali2.length +
                altreCausali3.length +
                altreCausali4.length +
                altreCausali5.length;
            this.causaliSelectCompleted = this.optionsList.length + this.selected;
        }

        this.filteredBanksMulti.next(this.optionsList.slice());
        this.bankMultiFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterBanksMulti();
            });

        /*this.bankMultiCtrl.valueChanges.pipe(takeUntil(this._onDestroy)).subscribe((el) => {
            console.log(el);
            if (el.length > 0) {
                console.log(this.bankMultiCtrl.value.length);
                if (el[0].id === 'selectAll') {
                    /!*this.selected = this.optionsList.length - 1;*!/
                } else {
                    const elementChecked = this.optionsList.find(element => element.id === el[0].id);
                    elementChecked.checked = !elementChecked.checked;
                    /!*              this.selected += 1;*!/
                }
            } else {
                this.selected = 0;
            }
        });*/
    }


    ngAfterViewInit() {
        this.setInitialValue();
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    /**
     * Sets the initial value after the filteredBanks are loaded initially
     */
    setInitialValue() {
        this.filteredBanksMulti.pipe(take(1), takeUntil(this._onDestroy))
            .subscribe(() => {
                this.multiSelect.compareWith = (a: CausaliInterface, b: CausaliInterface) => a && b && a.id === b.id;
            });
    }

    filterBanksMulti() {
        if (!this.optionsList) {
            return;
        }
        let search = this.bankMultiFilterCtrl.value;
        this.isCountVisible = false;
        if (!search) {
            this.isCountVisible = true;
            if (this.isCausali) {
                this.resetValues();
            }
            this.filteredBanksMulti.next(this.optionsList.slice());
            return;
        } else {
            search = search.toLowerCase();
            if (this.isCausali) {
                this.optionsList = [...this.causaliList, ...altreCausali1,
                    ...altreCausali2,
                    ...altreCausali3,
                    ...altreCausali4,
                    ...altreCausali5];
            }
        }

        this.filteredBanksMulti.next(
            this.optionsList.filter(bank => bank.name.toLowerCase().indexOf(search) > -1)
        );
    }

    toggleSelectAll() {
        if (this.bankMultiCtrl.value.find(bank => bank.id === 'selectAll')) {
            this.bankMultiCtrl.patchValue([{id: 'selectAll', name: ''}]);
        } else {
            this.bankMultiCtrl.patchValue([]);

        }
    }

    otherOptionSelect() {
        if (this.bankMultiCtrl.value.find(bank => bank.id === 'selectAll')) {
            const values = this.bankMultiCtrl.value;
            values.splice(values.findIndex(bank => bank.id === 'selectAll'), 1);
            this.bankMultiCtrl.patchValue(values);
        }
    }

    clear() {
        this.bankMultiCtrl.setValue([{id: 'selectAll', name: ''}]);
    }

    ngOnChanges(changes: SimpleChanges): void {
    }

    getMoreValues() {
        if (!this.isCausali) {
            return;
        }
        switch (this.optionsList.length) {
            case 9:
                this.optionsList.push(...altreCausali1);
                this.selected = this.selected - altreCausali1.length;

                break;
            case 15:
                this.optionsList.push(...altreCausali2);
                this.selected = this.selected - altreCausali2.length;

                break;
            case 21:
                this.optionsList.push(...altreCausali3);
                this.selected = this.selected - altreCausali3.length;

                break;
            case 27:
                this.optionsList.push(...altreCausali4);
                this.selected = this.selected - altreCausali4.length;

                break;
            case 33:
                this.optionsList.push(...altreCausali5);
                this.selected = this.selected - altreCausali5.length;
                break;
        }
        this.filteredBanksMulti.next(this.optionsList.slice());

    }

    resetValues() {
        if (!this.isCausali) {
            return;
        } else {
            this.optionsList = [...this.causaliList];
            this.filteredBanksMulti.next(this.optionsList.slice());
            this.selected = altreCausali1.length +
                altreCausali2.length +
                altreCausali3.length +
                altreCausali4.length +
                altreCausali5.length;
        }
    }


    isChecked(causale: CausaliInterface): boolean {
        if (this.bankMultiCtrl && this.bankMultiCtrl.value) {
            return this.bankMultiCtrl.value.find(obj => obj.id === causale.id);
        } else {
            return false;
        }
    }
}

export const altreCausali1 = [
    {name: 'Causale 1', id: 'J', checked: false},
    {name: 'Causale 2', id: 'K', checked: false},
    {name: 'Causale 3', id: 'L', checked: false},
    {name: 'Causale 4', id: 'N', checked: false},
    {name: 'Causale 5', id: 'O', checked: false},
    {name: 'Causale 6', id: 'P', checked: false}
];

export const altreCausali2 = [
    {name: 'Causale 7', id: 'Q', checked: false},
    {name: 'Causale 8', id: 'R', checked: false},
    {name: 'Causale 9', id: 'S', checked: false},
    {name: 'Causale 10', id: 'T', checked: false},
    {name: 'Causale 11', id: 'U', checked: false},
    {name: 'Causale 12', id: 'V', checked: false}
];
export const altreCausali3 = [
    {name: 'Causale 13', id: 'W', checked: false},
    {name: 'Causale 14', id: 'X', checked: false},
    {name: 'Causale 15', id: 'Y', checked: false},
    {name: 'Causale 16', id: 'Z', checked: false},
    {name: 'Causale 17', id: 'AB', checked: false},
    {name: 'Causale 18', id: 'BC', checked: false}
];
export const altreCausali4 = [
    {name: 'Causale 19', id: 'CD', checked: false},
    {name: 'Causale 20', id: 'EF', checked: false},
    {name: 'Causale 21', id: 'GH', checked: false},
    {name: 'Causale 22', id: 'IL', checked: false},
    {name: 'Causale 23', id: 'MN', checked: false},
    {name: 'Causale 24', id: 'OP', checked: false}
];
export const altreCausali5 = [
    {name: 'Causale 25', id: 'QR', checked: false},
    {name: 'Causale 26', id: 'ST', checked: false},
    {name: 'Causale 27', id: 'UV', checked: false},
    {name: 'Causale 28', id: 'WX', checked: false},
    {name: 'Causale 29', id: 'YZ', checked: false},
    {name: 'Causale 30', id: 'ABC', checked: false},
    {name: 'Causale 31', id: 'EFG', checked: false},
    {name: 'Causale 32', id: 'HIJ', checked: false}
];
