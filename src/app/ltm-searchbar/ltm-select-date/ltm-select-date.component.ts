import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {DatepikerDialogComponent} from '../datepiker-dialog/datepiker-dialog.component';
import {FormBuilder, FormGroup} from '@angular/forms';

export interface ViewDate {
    value: string;
    viewValue: string;
}

@Component({
    selector: 'app-ltm-select-date',
    templateUrl: './ltm-select-date.component.html',
    styleUrls: ['./ltm-select-date.component.scss']
})
export class LtmSelectDateComponent implements OnInit {

    @Input() isFullScreen = false;

    dateList: ViewDate[] = [
        {value: 'Ultime 24 ore', viewValue: 'Ultime 24 ore'},
        {value: 'Ultimi 7 giorni', viewValue: 'Ultimi 7 giorni'},
        {value: 'Ultimi 30 giorni', viewValue: 'Ultimi 30 giorni'},
        {value: 'Ultimi 3 mesi', viewValue: 'Ultimi 3 mesi'},
        {value: 'Ultimi 6 mesi', viewValue: 'Ultimi 6 mesi'},
        {value: 'Ultimi 12 mesi', viewValue: 'Ultimi 12 mesi*'}
    ];

    selectDate = null;
    form: FormGroup;

    constructor(public dialog: MatDialog,
                private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.form = this.fb.group({
            selectDate: ['']
        });
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(DatepikerDialogComponent, {
            width: 'auto',
            height: 'auto',
            autoFocus: false,
            data: {
                startDate: this.selectDate ? this.selectDate.split('-')[0].trim() : null,
                endDate: this.selectDate ? this.selectDate.split('-')[1].trim() : null
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result !== undefined) {
                if (result.date) {
                    this.selectDate = result.date;
                    setTimeout(() => {
                        this.form.get('selectDate').setValue(this.selectDate);
                    }, 5);

                }
            }
        });
    }

    clear() {
        this.form.get('selectDate').reset();
    }

    selectionChange(evt) {
        this.selectDate = null;
    }

}
