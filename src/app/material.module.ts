import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatInputModule} from '@angular/material/input';
import {MatChipsModule} from '@angular/material/chips';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatSelectInfiniteScrollModule} from 'ng-mat-select-infinite-scroll';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatInputModule,
        MatChipsModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatSelectInfiniteScrollModule
    ],
    providers: [
    ],
    exports: [
        CommonModule,
        MatInputModule,
        MatChipsModule,
        MatDialogModule,
        MatFormFieldModule,
        MatSelectModule,
        MatSelectInfiniteScrollModule
    ]
})
export class MaterialModule {
}
