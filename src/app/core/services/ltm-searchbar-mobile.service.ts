import {Injectable} from '@angular/core';

export interface EsitiChips {
    label: number;
    value: string;
    checked: boolean;
}

export interface Causali {
    label: number;
    value: string;
    checked: boolean;
}

export interface RangeDate {
    label: number;
    value: string;
    checked: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class LtmSearchbarMobileService {

    public causaliList: Causali[] = [
        {label: -1, value: 'Tutte le causali', checked: true},
        {label: 1, value: 'Bonus', checked: false},
        {label: 2, value: 'Deposito', checked: false},
        {label: 3, value: 'Account Management', checked: false},
        {label: 4, value: 'Giocata Bingo', checked: false},
        {label: 5, value: 'Giocata Casino', checked: false},
        {label: 6, value: 'Giocata Eurojackpot', checked: false}
    ];

    public esitiChips: EsitiChips[] = [
        {label: 1, value: 'Vincente', checked: false},
        {label: 2, value: 'Non vincente', checked: false},
        {label: 3, value: 'In corso', checked: false},
        {label: 4, value: 'Rimborsato', checked: false},
        {label: 5, value: 'Non rimborsato', checked: false}
    ];

    public rangeTemporale: RangeDate[] = [
        {label: 1, value: 'Ultime 24 ore', checked: false},
        {label: 2, value: 'Ultimi 7 giorni', checked: false},
        {label: 3, value: 'Ultimi 30 giorni', checked: false},
        {label: 4, value: 'Ultimi 3 mesi', checked: false},
        {label: 5, value: 'Ultimi 6 mesi', checked: false},
        {label: 6, value: '*Ultimi 12 mesi', checked: false},
    ];

    constructor() {
    }

    resetFilters() {
        this.causaliList = [
            {label: -1, value: 'Tutte le causali', checked: true},
            {label: 1, value: 'Bonus', checked: false},
            {label: 2, value: 'Deposito', checked: false},
            {label: 3, value: 'Account Management', checked: false},
            {label: 4, value: 'Giocata Bingo', checked: false},
            {label: 5, value: 'Giocata Casino', checked: false},
            {label: 6, value: 'Giocata Eurojackpot', checked: false}
        ];

        this.esitiChips = [
            {label: 1, value: 'Vincente', checked: false},
            {label: 2, value: 'Non vincente', checked: false},
            {label: 3, value: 'In corso', checked: false},
            {label: 4, value: 'Rimborsato', checked: false},
            {label: 5, value: 'Non rimborsato', checked: false}
        ];
    }
}
